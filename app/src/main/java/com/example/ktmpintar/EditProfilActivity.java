package com.example.ktmpintar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.api.APIRequestData;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ktmpintar.models.Mahasiswa.ResponseMahasiswa;
import com.example.ktmpintar.models.Mahasiswa.UpdateMhs;
import com.google.android.material.textfield.TextInputEditText;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfilActivity extends AppCompatActivity implements View.OnClickListener{
    private static final String TAG = "EditProfilActivity";
    private TextView btnBack;
    private TextInputEditText etNama, etNim, etKelas, etJurusan, etNotelf, etEmail;
    private Button btnUpdate;
    private String NamaMhs, Nim, Kelas, Jurusan, NoTelf, Email, no_ktm, token;
    private APIRequestData ardData;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_edit_profile);

        sessionManager = new SessionManager(EditProfilActivity.this);

        etNama = findViewById(R.id.edt_nama_mhs);
        etNim = findViewById(R.id.edt_nim);
        etKelas = findViewById(R.id.edt_kelas);
        etJurusan = findViewById(R.id.edt_jurusan);
        etNotelf = findViewById(R.id.edt_no_telfon);
        etEmail = findViewById(R.id.edt_email);
        btnUpdate = findViewById(R.id.btn_update_profil);
        btnBack = findViewById(R.id.back);

        no_ktm = sessionManager.getUserDetail().get(SessionManager.NO_KARTU);
        token = sessionManager.getUserDetail().get(SessionManager.TOKEN);

        btnBack.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponseMahasiswa> editProfileCall = ardData.getMhs(no_ktm, "Bearer "+ token);
        editProfileCall.enqueue(new Callback<ResponseMahasiswa>() {
            @Override
            public void onResponse(Call<ResponseMahasiswa> call, Response<ResponseMahasiswa> response) {
                etNama.setText(response.body().getData().get(0).getNama_mhs());
                etNim.setText(response.body().getData().get(0).getNim());
                etKelas.setText(response.body().getData().get(0).getKelas());
                etJurusan.setText(response.body().getData().get(0).getJurusan());
                etNotelf.setText(response.body().getData().get(0).getNo_telfon());
                etEmail.setText(response.body().getData().get(0).getEmail());
            }

            @Override
            public void onFailure(Call<ResponseMahasiswa> call, Throwable t) {
                Toast.makeText(EditProfilActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_update_profil:

                NamaMhs = etNama.getText().toString();
                Nim     = etNim.getText().toString();
                Kelas   = etKelas.getText().toString();
                Jurusan = etJurusan.getText().toString();
                NoTelf  = etNotelf.getText().toString();
                Email   = etEmail.getText().toString();

                updateProfil(NamaMhs,Nim,Kelas,Jurusan,NoTelf,Email);
                break;

            case R.id.back:
                super.onBackPressed();
                break;
        }
    }


    private void updateProfil(String NamaMhs, String Nim, String Kelas, String Jurusan, String NoTelf, String Email) {

        ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<UpdateMhs> updateMhsCall = ardData.updateProfil("Bearer "+ token, NamaMhs, Nim, Kelas, Jurusan, NoTelf, Email);
        updateMhsCall.enqueue(new Callback<UpdateMhs>() {
            @Override
            public void onResponse(Call<UpdateMhs> call, Response<UpdateMhs> response) {

                if (response.body() != null && response.isSuccessful() && !response.body().isError()) {

                    Toast.makeText(EditProfilActivity.this, "Update Profil Berhasil!", Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onCreate: " + token);
                    Intent intent = new Intent(EditProfilActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    try {
                        assert response.errorBody() != null;
                        Log.d(TAG, "response data = " + response.code() + response.errorBody().string());
                    } catch (IOException e) {
                        Log.d(TAG, "response data = " + e.getMessage());
                    }
                    Toast.makeText(EditProfilActivity.this, "Data Salah", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UpdateMhs> call, Throwable t) {
                Toast.makeText(EditProfilActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}

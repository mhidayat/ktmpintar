package com.example.ktmpintar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ktmpintar.adapter.AdapterKantin;
import com.example.ktmpintar.adapter.AdapterParkir;
import com.example.ktmpintar.api.APIRequestData;
import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.models.HistoryKantin.HistoryKantinModel;
import com.example.ktmpintar.models.HistoryKantin.ResponseHistoryKantin;
import com.example.ktmpintar.models.HistoryParkir.ResponseHistoryParkir;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KantinActivity extends AppCompatActivity implements View.OnClickListener{

  private RecyclerView rvHistoryKantin;
  private RecyclerView.Adapter adHistoryKantin;
  private RecyclerView.LayoutManager lmHistoryKantin;
  private List<HistoryKantinModel> listHistoryKantin = new ArrayList<>();
  private String token, saldo;
  private SessionManager sessionManager;
  private TextView btnBack, sisaSaldo;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_kantin);

    sessionManager = new SessionManager(KantinActivity.this);
    token = sessionManager.getUserDetail().get(SessionManager.TOKEN);
    saldo = sessionManager.getUserDetail().get(SessionManager.SALDO);
    btnBack = findViewById(R.id.back_kantin_act);
    sisaSaldo = findViewById(R.id.sisaSaldo);
    rvHistoryKantin = findViewById(R.id.rvHistoryKantin);
    lmHistoryKantin = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    rvHistoryKantin.setLayoutManager(lmHistoryKantin);
    retrieveDataHistoryKantin();

    btnBack.setOnClickListener(this);

  }

  private void retrieveDataHistoryKantin() {
    APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
    Call<ResponseHistoryKantin> tampilHistoryKantin = ardData.getHistoryKantin("Bearer "+ token);
    tampilHistoryKantin.enqueue(new Callback<ResponseHistoryKantin>() {
      @Override
      public void onResponse(Call<ResponseHistoryKantin> call, Response<ResponseHistoryKantin> response) {

        sisaSaldo.setText(response.body().getSisaSaldo().get(0).getSaldo());
        listHistoryKantin = response.body().getData();

        adHistoryKantin = new AdapterKantin(KantinActivity.this, listHistoryKantin);
        rvHistoryKantin.setAdapter(adHistoryKantin);
        adHistoryKantin.notifyDataSetChanged();
      }

      @Override
      public void onFailure(Call<ResponseHistoryKantin> call, Throwable t) {
        Toast.makeText(KantinActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.back_kantin_act:
        super.onBackPressed();
        break;
    }
  }
}
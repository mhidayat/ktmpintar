package com.example.ktmpintar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.api.APIRequestData;
import com.example.ktmpintar.models.Login.Login;
import com.example.ktmpintar.models.Login.LoginData;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
  private static final String TAG = "LoginActivity";
  private TextInputEditText etNoktm;
  private TextView btnLoginPenjual;
  private Button btnLogin;
  private String NoKtm;
  private APIRequestData ardData;
  private SessionManager sessionManager;


  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_login_card);

    etNoktm = findViewById(R.id.edt_no_kartu_login);
    btnLogin = findViewById(R.id.btn_login_no_kartu);
    btnLoginPenjual = findViewById(R.id.login_penjual);

    btnLogin.setOnClickListener(this);
    btnLoginPenjual.setOnClickListener(this);
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.btn_login_no_kartu:
        NoKtm = etNoktm.getText().toString();
        login(NoKtm);
        break;

      case R.id.login_penjual:
        Intent intent = new Intent(LoginActivity.this, LoginPenjualActivity.class);
        startActivity(intent);
        break;
    }
  }

  private void login(String noKtm) {
    ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
    Call<Login> logincardCall = ardData.logincardResponse(noKtm);
    logincardCall.enqueue(new Callback<Login>() {
      @Override
      public void onResponse(Call<Login> call, Response<Login> response) {
        if (response.body() != null && response.isSuccessful() && !response.body().isError()) {
          Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
          sessionManager = new SessionManager(LoginActivity.this);
          LoginData loginData = response.body().getLoginData();
          sessionManager.createLoginSession(loginData);
          Log.d(TAG, "onResponse: " + response.body().getLoginData());
          Intent intent = new Intent(LoginActivity.this, MainActivity.class);
          startActivity(intent);
          finish();
        } else {
          Toast.makeText(LoginActivity.this, "Masukkan Nomor Kartu / Nomor Kartu tidak ditemukan", Toast.LENGTH_LONG).show();
        }

      }

      @Override
      public void onFailure(Call<Login> call, Throwable t) {
        Toast.makeText(LoginActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
      }
    });



//    Intent intent = new Intent(this, MainActivity.class);
//    startActivity(intent);
  }
}
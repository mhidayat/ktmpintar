package com.example.ktmpintar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.ktmpintar.models.Login.LoginDataPenjual;
import com.example.ktmpintar.models.Login.LoginPenjual;
import com.google.android.material.textfield.TextInputEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPenjualActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "LoginPenjualActivity";
    private TextInputEditText etIdPenjual;
    private TextView btnLoginMhs;
    private Button btnLoginPenjual;
    private String IdPenjual;
    private com.example.ktmpintar.api.APIRequestData ardData;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login_penjual);

        etIdPenjual = findViewById(R.id.edt_id_penjual_login);
        btnLoginPenjual = findViewById(R.id.btn_login_penjual);
        btnLoginMhs = findViewById(R.id.login_mhs);

        btnLoginPenjual.setOnClickListener(this);
        btnLoginMhs.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login_penjual:
                IdPenjual = etIdPenjual.getText().toString();
                loginPenjual(IdPenjual);
                break;

            case R.id.login_mhs:
                Intent intent = new Intent(LoginPenjualActivity.this, LoginActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void loginPenjual(String idPenjual) {
        ardData = com.example.ktmpintar.api.RetroServer.konekRetrofit().create(com.example.ktmpintar.api.APIRequestData.class);
        Call<LoginPenjual> loginPenjualCall = ardData.loginpenjualResponse(idPenjual);
        loginPenjualCall.enqueue(new Callback<LoginPenjual>() {
            @Override
            public void onResponse(Call<LoginPenjual> call, Response<LoginPenjual> response) {
                if (response.body() != null && response.isSuccessful() && !response.body().isError()) {
                    Toast.makeText(LoginPenjualActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    sessionManager = new SessionManager(LoginPenjualActivity.this);
                    LoginDataPenjual loginDataPenjual = response.body().getLoginDataPenjual();
                    sessionManager.createLoginPenjualSession(loginDataPenjual);
                    Log.d(TAG, "onResponse: " + response.body().getLoginDataPenjual());
                    Intent intent = new Intent(LoginPenjualActivity.this, PenjualActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(LoginPenjualActivity.this, "Masukkan Id Penjual / Id Penjual tidak ditemukan", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginPenjual> call, Throwable t) {
                Toast.makeText(LoginPenjualActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


}

package com.example.ktmpintar;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ktmpintar.api.APIRequestData;
import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.models.Mahasiswa.ResponseMahasiswa;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
  private static final String TAG = "MainActivity";
  private SessionManager sessionManager;
  private String nama_mhs, no_ktm, saldo, token;
  private TextView tvNama, tvKtm, tvSaldo, tvTopup;
  private ImageView tvLoadMhs;
  private Button btLogout, btn_bayar_dana, btnWa;
  private CardView btnProfile, btnParkir, btnKantin;

  String dana_link = "https://link.dana.id/qr/481q4pfw";
  String wa_num  = "6281311489847";
  String wa_text = "Saya ingin mengirim bukti transfer";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_main);

    sessionManager = new SessionManager(MainActivity.this);
    if (!sessionManager.isLoggedin()){
      moveToLogin();
    }

    tvNama = findViewById(R.id.tv_nama_mhs);
    tvKtm = findViewById(R.id.tv_no_kartu);
    tvSaldo = findViewById(R.id.tv_saldo);
    tvTopup = findViewById(R.id.tv_topup);
    tvLoadMhs = findViewById(R.id.tv_loadMhs);
    btLogout = findViewById(R.id.btn_logout);
    btn_bayar_dana = findViewById(R.id.btn_bayar_dana);
    btnProfile = findViewById(R.id.menu_profil);
    btnParkir = findViewById(R.id.menu_parkir);
    btnKantin = findViewById(R.id.menu_kantin);
    btnWa = findViewById(R.id.btn_wa);

    nama_mhs = sessionManager.getUserDetail().get(SessionManager.NAMA_MHS);
    no_ktm = sessionManager.getUserDetail().get(SessionManager.NO_KARTU);
    saldo = sessionManager.getUserDetail().get(SessionManager.SALDO);
    token = sessionManager.getUserDetail().get(SessionManager.TOKEN);

    Log.d(TAG, "onCreate: " + token);

    tvNama.setText(nama_mhs);
    tvKtm.setText(no_ktm);
    tvSaldo.setText(saldo);

    btLogout.setOnClickListener(this);
    btn_bayar_dana.setOnClickListener(this);
    btnProfile.setOnClickListener(this);
    btnParkir.setOnClickListener(this);
    btnKantin.setOnClickListener(this);
    tvTopup.setOnClickListener(this);
    tvLoadMhs.setOnClickListener(this);
    btnWa.setOnClickListener(this);

  }

  private void moveToLogin() {
    Intent intent = new Intent(MainActivity.this, LoginActivity.class);
    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
    startActivity(intent);
    finish();
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.btn_logout:
        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setMessage("Yakin ingin keluar dari akun ini?");
        builder.setPositiveButton("OK", (dialog, which) -> {
          sessionManager.logoutSession();
          moveToLogin();
          Toast.makeText(this, "Anda telah logout", Toast.LENGTH_SHORT).show();
        });
        builder.setNegativeButton("Cancel", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        break;

      case R.id.menu_profil:
        Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
        startActivity(intent);
        break;

      case R.id.menu_parkir:
        Intent intentparkir = new Intent(MainActivity.this, ParkirActivity.class);
        startActivity(intentparkir);
        break;

      case R.id.menu_kantin:
        Intent intentKantin = new Intent(MainActivity.this, KantinActivity.class);
        startActivity(intentKantin);
        break;

      case R.id.tv_topup:
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(
                MainActivity.this, R.style.Theme_Design_BottomSheetDialog
        );
        View bottomSheetView = LayoutInflater.from(getApplicationContext())
                .inflate(
                        R.layout.layout_bottom_sheet,
                        (LinearLayout)findViewById(R.id.bottomSheetContainer)
                );
        bottomSheetDialog.setContentView(bottomSheetView);
        bottomSheetDialog.show();
        break;

      case R.id.btn_bayar_dana:
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dana_link));
        startActivity(browserIntent);
        break;

      case R.id.tv_loadMhs:
        loadSaldo();
        break;

      case R.id.btn_wa:

          boolean installed = isAppInstalled("com.whatsapp");

          if (installed) {
              Intent intentWa = new Intent(Intent.ACTION_VIEW);
              intentWa.setData(Uri.parse("http://api.whatsapp.com/send?phone="+wa_num+"&text="+wa_text));
              startActivity(intentWa);
          } else {
              Toast.makeText(MainActivity.this, "Whatsapp belum terinstall", Toast.LENGTH_SHORT);
          }

        break;
    }
  }

    private boolean isAppInstalled(String s) {
        PackageManager packageManager = getPackageManager();
        boolean is_installed;

        try {
            packageManager.getPackageInfo(s, PackageManager.GET_ACTIVITIES);
            is_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            is_installed = false;
            e.printStackTrace();
        }
        return is_installed;
    }

    private void loadSaldo() {

    APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
    Call<ResponseMahasiswa> profileCall = ardData.getMhs("Bearer "+ token);
    profileCall.enqueue(new Callback<ResponseMahasiswa>() {
      @Override
      public void onResponse(Call<ResponseMahasiswa> call, Response<ResponseMahasiswa> response) {
        String saldo = response.body().getData().get(0).getSaldo();
        String namaMhs = response.body().getData().get(0).getNama_mhs();
        tvSaldo.setText(saldo);
        tvNama.setText(namaMhs);
        sessionManager = new SessionManager(getApplicationContext());
        sessionManager.saveUpdateSession(saldo, namaMhs);
        Toast.makeText(getApplicationContext(),"Sukses", Toast.LENGTH_SHORT).show();
        Log.d(TAG, " Saldo --> "+ saldo);
      }

      @Override
      public void onFailure(Call<ResponseMahasiswa> call, Throwable t) {
        Log.d(TAG, "Failure Saldo --> "+ t.getMessage());
        Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
      }
    });

  }
}
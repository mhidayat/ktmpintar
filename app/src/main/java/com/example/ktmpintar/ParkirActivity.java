package com.example.ktmpintar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ktmpintar.adapter.AdapterParkir;
import com.example.ktmpintar.api.APIRequestData;
import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.models.HistoryParkir.HistoryParkirModel;
import com.example.ktmpintar.models.HistoryParkir.ResponseHistoryParkir;
import com.example.ktmpintar.models.HistoryParkir.StatusParkir;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ParkirActivity extends AppCompatActivity implements View.OnClickListener {

  private RecyclerView rvHistoryParkir;
  private RecyclerView.Adapter adHistoryParkir;
  private RecyclerView.LayoutManager lmHistoryParkir;
  private List<HistoryParkirModel> listHistoryParkir = new ArrayList<>();
  private String token;
  private SessionManager sessionManager;
  private TextView btnBack, statusParkir, tvEmpty;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_parkir);

    sessionManager = new SessionManager(ParkirActivity.this);
    token = sessionManager.getUserDetail().get(SessionManager.TOKEN);
    tvEmpty = findViewById(R.id.tv_nodata);
    btnBack = findViewById(R.id.back_parkir_act);
    statusParkir = findViewById(R.id.statusParkir);
    rvHistoryParkir = findViewById(R.id.rvHistoryParkir);
    lmHistoryParkir = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
    rvHistoryParkir.setLayoutManager(lmHistoryParkir);
    retrieveDataHistoryParkir();

    btnBack.setOnClickListener(this);
  }

  public void retrieveDataHistoryParkir(){
    APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
    Call<ResponseHistoryParkir> tampilHistoryParkir = ardData.getHistoryParkir("Bearer "+ token);
    tampilHistoryParkir.enqueue(new Callback<ResponseHistoryParkir>() {
      @Override
      public void onResponse(Call<ResponseHistoryParkir> call, Response<ResponseHistoryParkir> response) {

        if (response.body().getStatusParkir().get(0).getStatus() == null) {
          statusParkir.setText("Tidak Terparkir");
          } else statusParkir.setText(response.body().getStatusParkir().get(0).getStatus());

        listHistoryParkir = response.body().getData();

        if (listHistoryParkir.isEmpty()) {
          tvEmpty.setVisibility(View.VISIBLE);
        } else {
          adHistoryParkir = new AdapterParkir(ParkirActivity.this, listHistoryParkir);
          rvHistoryParkir.setAdapter(adHistoryParkir);
          adHistoryParkir.notifyDataSetChanged();
          tvEmpty.setVisibility(View.GONE);
        }
      }

      @Override
      public void onFailure(Call<ResponseHistoryParkir> call, Throwable t) {
        Toast.makeText(ParkirActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.back_parkir_act:
        super.onBackPressed();
        break;
    }
  }
}
package com.example.ktmpintar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktmpintar.adapter.AdapterPenjual;
import com.example.ktmpintar.api.APIRequestData;
import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.models.Mahasiswa.ResponseMahasiswa;
import com.example.ktmpintar.models.Penjual.PenjualModel;
import com.example.ktmpintar.models.Penjual.ResponsePenjual;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PenjualActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvHistoryPenjual;
    private RecyclerView.Adapter adHistoryPenjual;
    private RecyclerView.LayoutManager lmHistoryPenjual;
    private List<PenjualModel> listHistoryPenjual = new ArrayList<>();
    private static final String TAG = "PenjualActivity";
    private SessionManager sessionManager;
    private String nama_warung, kode_kantin, saldo_penjual, token;
    private TextView tvNamaWarung, tvIdPenjual, tvSaldoPenjual;
    private ImageView tv_loadPenjual;
    private Button btnLogoutPenjual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_penjual);

        sessionManager = new SessionManager(PenjualActivity.this);
        if (!sessionManager.isLoggedin()){
            moveToLogin();
        }

        tvNamaWarung = findViewById(R.id.tv_nama_warung);
        tvIdPenjual = findViewById(R.id.tv_id_penjual);
        tvSaldoPenjual = findViewById(R.id.tv_saldo_penjual);
        tv_loadPenjual = findViewById(R.id.tv_loadPenjual);
        btnLogoutPenjual = findViewById(R.id.btn_logout_penjual);

        nama_warung = sessionManager.getPenjualDetail().get(SessionManager.NAMA_WARUNG);
        kode_kantin = sessionManager.getPenjualDetail().get(SessionManager.KODE_KANTIN);
        token = sessionManager.getPenjualDetail().get(SessionManager.TOKEN);

        Log.d(TAG, "onCreate: " + token);

        tvNamaWarung.setText(nama_warung);
        tvIdPenjual.setText(kode_kantin);

        rvHistoryPenjual = findViewById(R.id.rvHistoryPenjual);
        lmHistoryPenjual = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvHistoryPenjual.setLayoutManager(lmHistoryPenjual);
        retrieveDataHistoryPenjual();

        btnLogoutPenjual.setOnClickListener(this);
        tv_loadPenjual.setOnClickListener(this);

    }

    private void retrieveDataHistoryPenjual() {
        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponsePenjual> tampilHistoryPenjual = ardData.getPenjual("Bearer "+ token);
        tampilHistoryPenjual.enqueue(new Callback<ResponsePenjual>() {
            @Override
            public void onResponse(Call<ResponsePenjual> call, Response<ResponsePenjual> response) {
                tvSaldoPenjual.setText(response.body().getSisaSaldoPenjual().get(0).getSaldoPenjual());
                listHistoryPenjual = response.body().getData();

                adHistoryPenjual = new AdapterPenjual(PenjualActivity.this, listHistoryPenjual);
                rvHistoryPenjual.setAdapter(adHistoryPenjual);
                adHistoryPenjual.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<ResponsePenjual> call, Throwable t) {
                Toast.makeText(PenjualActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void moveToLogin() {
        Intent intent = new Intent(PenjualActivity.this, LoginPenjualActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_logout_penjual:
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setMessage("Yakin ingin keluar dari akun ini?");
                builder.setPositiveButton("OK", (dialog, which) -> {
                    sessionManager.logoutSession();
                    moveToLogin();
                    Toast.makeText(this, "Anda telah logout", Toast.LENGTH_SHORT).show();
                });
                builder.setNegativeButton("Cancel", null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
                break;

            case R.id.tv_loadPenjual:
                loadData();
                break;
        }
    }

    private void loadData() {

        APIRequestData ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
        Call<ResponsePenjual> profileCall = ardData.getPenjual("Bearer "+ token);
        profileCall.enqueue(new Callback<ResponsePenjual>() {
            @Override
            public void onResponse(Call<ResponsePenjual> call, Response<ResponsePenjual> response) {
                String namaWarung = response.body().getData().get(0).getNamaWarung();
                String saldoPenjual = response.body().getSisaSaldoPenjual().get(0).getSaldoPenjual();
                tvNamaWarung.setText(namaWarung);
                tvSaldoPenjual.setText(saldoPenjual);
                sessionManager = new SessionManager(getApplicationContext());
                sessionManager.savePenjualSession(namaWarung, saldoPenjual);
                Toast.makeText(getApplicationContext(),"Sukses", Toast.LENGTH_SHORT).show();
                Log.d(TAG, " Saldo --> "+ saldoPenjual);
            }

            @Override
            public void onFailure(Call<ResponsePenjual> call, Throwable t) {
                Log.d(TAG, "Failure Saldo --> "+ t.getMessage());
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}

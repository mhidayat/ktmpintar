package com.example.ktmpintar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ktmpintar.api.APIRequestData;
import com.example.ktmpintar.api.RetroServer;
import com.example.ktmpintar.models.Login.Login;
import com.example.ktmpintar.models.Mahasiswa.ResponseMahasiswa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener{
  public static final String EXTRA_TEXT = "com.example.application.example.EXTRA_TEXT";
  private static final String TAG = "ProfileActivity";
  private TextView tvNama, tvNim, tvKelas, tvJurusan, tvTelepon, tvEmail, btnBack;
  private Button btnUpdate;
  private String no_ktm, token, namaMhs, nim, kelas, jurusan, noTelfon, email;
  private APIRequestData ardData;
  private SessionManager sessionManager;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_profile);

    sessionManager = new SessionManager(ProfileActivity.this);

    tvNama = findViewById(R.id.edName);
    tvNim = findViewById(R.id.edNim);
    tvKelas = findViewById(R.id.edKelas);
    tvJurusan = findViewById(R.id.edJurusan);
    tvTelepon = findViewById(R.id.edNoTelf);
    tvEmail = findViewById(R.id.edEmail);
    btnUpdate = findViewById(R.id.btn_edit_profil);
    btnBack = findViewById(R.id.back);

    no_ktm = sessionManager.getUserDetail().get(SessionManager.NO_KARTU);
    token = sessionManager.getUserDetail().get(SessionManager.TOKEN);

    btnUpdate.setOnClickListener(this);
    btnBack.setOnClickListener(this);

    ardData = RetroServer.konekRetrofit().create(APIRequestData.class);
    Call<ResponseMahasiswa> profileCall = ardData.getMhs(no_ktm, "Bearer "+ token);
    profileCall.enqueue(new Callback<ResponseMahasiswa>() {
      @Override
      public void onResponse(Call<ResponseMahasiswa> call, Response<ResponseMahasiswa> response) {
        tvNama.setText(response.body().getData().get(0).getNama_mhs());
        tvNim.setText(response.body().getData().get(0).getNim());
        tvKelas.setText(response.body().getData().get(0).getKelas());
        tvJurusan.setText(response.body().getData().get(0).getJurusan());
        tvTelepon.setText(response.body().getData().get(0).getNo_telfon());
        tvEmail.setText(response.body().getData().get(0).getEmail());
      }

      @Override
      public void onFailure(Call<ResponseMahasiswa> call, Throwable t) {
        Toast.makeText(ProfileActivity.this, t.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
      }
    });
  }

  @Override
  public void onClick(View v) {
    switch (v.getId()){
      case R.id.btn_edit_profil:
        String namaMhs = tvNama.getText().toString();
        String nim = tvNim.getText().toString();
        String kelas = tvKelas.getText().toString();
        String jurusan = tvJurusan.getText().toString();
        String noTelfon = tvTelepon.getText().toString();
        String email = tvEmail.getText().toString();

        Intent intent = new Intent(ProfileActivity.this, EditProfilActivity.class);
        intent.putExtra(EXTRA_TEXT, namaMhs);
        intent.putExtra(EXTRA_TEXT, nim);
        intent.putExtra(EXTRA_TEXT, kelas);
        intent.putExtra(EXTRA_TEXT, jurusan);
        intent.putExtra(EXTRA_TEXT, noTelfon);
        intent.putExtra(EXTRA_TEXT, email);
        startActivity(intent);
        break;

      case R.id.back:
        super.onBackPressed();
        break;
    }
  }
}
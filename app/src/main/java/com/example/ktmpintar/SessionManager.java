package com.example.ktmpintar;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.ktmpintar.models.Login.LoginData;
import com.example.ktmpintar.models.Login.LoginDataPenjual;
import com.example.ktmpintar.models.Mahasiswa.EditMhsData;

import java.util.HashMap;

public class SessionManager {
  private final Context context;
  private final SharedPreferences sharedPreferences;
  private final SharedPreferences.Editor editor;

  public static final String IS_LOGGED_IN = "isLoggedIn";
  public static final String ID = "id";
  public static final String NAMA_MHS = "nama_mhs";
  public static final String NO_KARTU = "no_kartu";
  public static final String NIM = "nim";
  public static final String KELAS = "kelas";
  public static final String JURUSAN = "jurusan";
  public static final String NO_TELFON = "no_telfon";
  public static final String EMAIL = "email";
  public static final String SALDO = "saldo";
  public static final String TOKEN = "token";

  public static final String ID_PENJUAL = "id";
  public static final String NAMA_WARUNG = "nama_warung";
  public static final String KODE_KANTIN = "kode_kantin";
  public static final String SALDO_PENJUAL = "saldo_penjual";


  public SessionManager(Context context) {
    this.context = context;
    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    editor = sharedPreferences.edit();
  }

  public void saveUpdateSession(String saldo, String namaMhs) {
    editor.putString(SALDO, saldo);
    editor.putString(NAMA_MHS, namaMhs);
    editor.commit();
  }

  public void savePenjualSession(String namaWarung, String saldoPenjual) {
    editor.putString(NAMA_WARUNG, namaWarung);
    editor.putString(SALDO, saldoPenjual);
    editor.commit();
  }

  public void createLoginSession(LoginData user) {
    editor.putBoolean(IS_LOGGED_IN, true);
    editor.putString(ID, user.getId());
    editor.putString(NAMA_MHS, user.getNama_mhs());
    editor.putString(NO_KARTU, user.getNo_kartu());
    editor.putString(NIM, user.getNim());
    editor.putString(JURUSAN, user.getJurusan());
    editor.putString(SALDO, user.getSaldo());
    editor.putString(TOKEN, user.getToken());
    editor.commit();
  }

  public void createLoginPenjualSession(LoginDataPenjual user) {
    editor.putBoolean(IS_LOGGED_IN, true);
    editor.putString(ID, user.getId());
    editor.putString(ID_PENJUAL, user.getId_penjual());
    editor.putString(KODE_KANTIN, user.getKode_kantin());
    editor.putString(NAMA_WARUNG, user.getNama_warung());
    editor.putString(SALDO_PENJUAL, user.getSaldo_penjual());
    editor.putString(TOKEN, user.getToken());
    editor.commit();
  }

  public void updateProfilSession(EditMhsData mhs) {
    editor.putBoolean(IS_LOGGED_IN, true);
    editor.putString(ID, mhs.getId());
    editor.putString(NAMA_MHS, mhs.getNama_mhs());
    editor.putString(NIM, mhs.getNim());
    editor.putString(KELAS, mhs.getKelas());
    editor.putString(JURUSAN, mhs.getJurusan());
    editor.putString(NO_TELFON, mhs.getNo_telfon());
    editor.putString(EMAIL, mhs.getEmail());
    editor.putString(TOKEN, mhs.getToken());
    editor.commit();
  }

  public HashMap<String, String> getUserDetail() {
    HashMap<String, String> user = new HashMap<>();
    user.put(ID, sharedPreferences.getString(ID, null));
    user.put(NAMA_MHS, sharedPreferences.getString(NAMA_MHS, null));
    user.put(NO_KARTU, sharedPreferences.getString(NO_KARTU, null));
    user.put(NIM, sharedPreferences.getString(NIM, null));
    user.put(JURUSAN, sharedPreferences.getString(JURUSAN, null));
    user.put(SALDO, sharedPreferences.getString(SALDO, null));
    user.put(TOKEN, sharedPreferences.getString(TOKEN, null));
    return user;
  }

  public HashMap<String, String> getPenjualDetail() {
    HashMap<String, String> penjual = new HashMap<>();
    penjual.put(ID, sharedPreferences.getString(ID, null));
    penjual.put(ID_PENJUAL, sharedPreferences.getString(ID_PENJUAL, null));
    penjual.put(KODE_KANTIN, sharedPreferences.getString(KODE_KANTIN, null));
    penjual.put(NAMA_WARUNG, sharedPreferences.getString(NAMA_WARUNG, null));
    penjual.put(SALDO_PENJUAL, sharedPreferences.getString(SALDO_PENJUAL, null));
    penjual.put(NIM, sharedPreferences.getString(NIM, null));
    penjual.put(TOKEN, sharedPreferences.getString(TOKEN, null));
    return penjual;
  }

  public void logoutSession() {
    editor.clear();
    editor.commit();
  }

  public boolean isLoggedin() {
    return sharedPreferences.getBoolean(IS_LOGGED_IN, false);
  }

}

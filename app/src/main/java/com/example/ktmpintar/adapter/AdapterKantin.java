package com.example.ktmpintar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktmpintar.R;
import com.example.ktmpintar.models.HistoryKantin.HistoryKantinModel;

import java.util.List;

public class AdapterKantin extends  RecyclerView.Adapter<AdapterKantin.HolderDataKantin>{
  private Context context;
  private List<HistoryKantinModel> listHistoryKantin;

  public AdapterKantin(Context context, List<HistoryKantinModel> listHistoryKantin) {
    this.context = context;
    this.listHistoryKantin = listHistoryKantin;
  }

  @NonNull
  @Override
  public HolderDataKantin onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_kantin, parent, false);
    HolderDataKantin holder = new HolderDataKantin(layout);
    return holder;
  }

  @Override
  public void onBindViewHolder(@NonNull AdapterKantin.HolderDataKantin holder, int position) {
    HistoryKantinModel hkm = listHistoryKantin.get(position);
    holder.tvIdHistoryKantin.setText(String.valueOf(hkm.getId()));
    holder.tvWaktuTransaksi.setText(hkm.getJam_transaksi());
  }

  @Override
  public int getItemCount() {
    return listHistoryKantin.size();
  }

  public class HolderDataKantin extends RecyclerView.ViewHolder {
    TextView tvIdHistoryKantin, tvWaktuTransaksi, tvSaldoTerpakai;

    public HolderDataKantin(@NonNull View itemView) {
      super(itemView);

      tvIdHistoryKantin = itemView.findViewById(R.id.tvIdHistoryKantin);
      tvWaktuTransaksi = itemView.findViewById(R.id.waktuTransaksi);
      tvSaldoTerpakai = itemView.findViewById(R.id.saldoTerpakai);
    }
  }
}

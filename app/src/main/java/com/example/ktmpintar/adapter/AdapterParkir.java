package com.example.ktmpintar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktmpintar.R;
import com.example.ktmpintar.models.HistoryParkir.HistoryParkirModel;

import java.util.List;

public class AdapterParkir  extends RecyclerView.Adapter<AdapterParkir.HolderDataParkir>{
  private Context context;
  private List<HistoryParkirModel> listHistoryParkir;

  public AdapterParkir(Context context, List<HistoryParkirModel> listHistoryParkir) {
    this.context = context;
    this.listHistoryParkir = listHistoryParkir;
  }

  @NonNull
  @Override
  public HolderDataParkir onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_parkir, parent, false);
    HolderDataParkir holder = new HolderDataParkir(layout);
    return holder;
  }

  @Override
  public void onBindViewHolder(@NonNull AdapterParkir.HolderDataParkir holder, int position) {
    HistoryParkirModel hpm = listHistoryParkir.get(position);
    holder.tvIdHistoryParkir.setText(String.valueOf(hpm.getId()));
    holder.tvJamMasuk.setText(hpm.getJam_masuk());
    holder.tvJamKeluar.setText(hpm.getJam_keluar());
  }

  @Override
  public int getItemCount() {
    return listHistoryParkir.size();
  }

  public class HolderDataParkir extends RecyclerView.ViewHolder {
    TextView tvIdHistoryParkir, tvJamMasuk, tvJamKeluar, tvSaldoTerpakai;

    public HolderDataParkir(@NonNull View itemView) {
      super(itemView);

      tvIdHistoryParkir = itemView.findViewById(R.id.tvIdHistoryParkir);
      tvJamMasuk = itemView.findViewById(R.id.jamMasuk);
      tvJamKeluar = itemView.findViewById(R.id.jamKeluar);
      tvSaldoTerpakai = itemView.findViewById(R.id.saldoTerpakai);
    }
  }
}

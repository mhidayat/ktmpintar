package com.example.ktmpintar.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktmpintar.R;
import com.example.ktmpintar.models.Penjual.PenjualModel;

import java.util.List;

public class AdapterPenjual extends RecyclerView.Adapter<AdapterPenjual.HolderDataPenjual>{
    private final Context context;
    private final List<PenjualModel> listHistoryPenjual;

    public AdapterPenjual(Context context, List<PenjualModel> listHistoryPenjual) {
        this.context = context;
        this.listHistoryPenjual = listHistoryPenjual;
    }
    @NonNull
    @Override
    public HolderDataPenjual onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_penjual, parent, false);
        HolderDataPenjual holder = new HolderDataPenjual(layout);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterPenjual.HolderDataPenjual holder, int position) {
        PenjualModel pm = listHistoryPenjual.get(position);
        holder.idPenjual.setText(pm.getNamaWarung());
        holder.tvHistoryPenjual.setText(String.valueOf(pm.getId()));
        holder.transaksiPenjual.setText(pm.getJam_transaksi());
    }

    @Override
    public int getItemCount() {
        return listHistoryPenjual.size();
    }

    public class HolderDataPenjual extends RecyclerView.ViewHolder {
        TextView tvHistoryPenjual, transaksiPenjual, idPenjual;

        public HolderDataPenjual(@NonNull View itemView) {
            super(itemView);

            tvHistoryPenjual = itemView.findViewById(R.id.tvHistoryPenjual);
            transaksiPenjual = itemView.findViewById(R.id.transaksiPenjual);
            idPenjual    = itemView.findViewById(R.id.id_penjual);
        }
    }
}

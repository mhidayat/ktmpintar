package com.example.ktmpintar.api;

import com.example.ktmpintar.models.HistoryKantin.ResponseHistoryKantin;
import com.example.ktmpintar.models.HistoryParkir.ResponseHistoryParkir;
import com.example.ktmpintar.models.Login.Login;
import com.example.ktmpintar.models.Login.LoginPenjual;
import com.example.ktmpintar.models.Mahasiswa.ResponseMahasiswa;
import com.example.ktmpintar.models.Mahasiswa.UpdateMhs;
import com.example.ktmpintar.models.Penjual.ResponsePenjual;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIRequestData {

  @FormUrlEncoded
  @POST("logincard")
  Call<Login> logincardResponse(
          @Field("no_kartu") String no_kartu
  );

  @FormUrlEncoded
  @POST("loginpenjual")
  Call<LoginPenjual> loginpenjualResponse(
          @Field("kode_kantin") String kode_kantin
  );

  @Headers({ "Content-Type: application/json;charset=UTF-8"})
  @GET("mahasiswa")
  Call<ResponseMahasiswa> getMhs(@Query("no_kartu") String no_kartu, @Header("Authorization") String auth);

  @GET("mahasiswa")
  Call<ResponseMahasiswa> getMhs( @Header("Authorization") String auth);


  @Headers({ "Content-Type: application/json;charset=UTF-8"})
  @GET("mahasiswa/history_parkir")
  Call<ResponseHistoryParkir> getHistoryParkir(@Header("Authorization") String auth);

  @Headers({ "Content-Type: application/json;charset=UTF-8"})
  @GET("mahasiswa/history_kantin")
  Call<ResponseHistoryKantin> getHistoryKantin(@Header("Authorization") String auth);
//  @FormUrlEncoded
//  @POST("user/edit")
//  Call<User> updateUser(@Field("first_name") String first, @Field("last_name") String last);

  //@Headers({ "Content-Type: application/json;charset=UTF-8"})
  @FormUrlEncoded
  @POST("mahasiswa/update_profil")
  Call<UpdateMhs> updateProfil(
          @Header("Authorization") String auth,
          @Field("nama_mhs") String nama_mhs,
          @Field("nim") String nim,
          @Field("kelas") String kelas,
          @Field("jurusan") String jurusan,
          @Field("no_telfon") String no_telfon,
          @Field("email") String email
  );

  @Headers({ "Content-Type: application/json;charset=UTF-8"})
  @GET("penjual")
  Call<ResponsePenjual> getPenjual(@Header("Authorization") String auth);


}

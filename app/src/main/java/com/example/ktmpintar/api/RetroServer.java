package com.example.ktmpintar.api;

import com.example.ktmpintar.Config;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroServer {
  private static final String BASE_URL = Config.BASE_URL;
  private static Retrofit retro;

  public static Retrofit konekRetrofit() {
    if (retro == null){
      retro = new Retrofit.Builder()
              .baseUrl(BASE_URL)
              .addConverterFactory(GsonConverterFactory.create())
              .build();
    }
    return  retro;
  }
}

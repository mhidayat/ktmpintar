package com.example.ktmpintar.models.HistoryKantin;

import com.example.ktmpintar.models.HistoryParkir.StatusParkir;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseHistoryKantin {
  @SerializedName("Error")
  private boolean error;

  @SerializedName("Data")
  private List<HistoryKantinModel> data;

  @SerializedName("Saldo")
  private List<SisaSaldo> sisaSaldo;

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public List<HistoryKantinModel> getData() {
    return data;
  }

  public void setData(List<HistoryKantinModel> data) {
    this.data = data;
  }

  public List<SisaSaldo> getSisaSaldo() {
    return sisaSaldo;
  }

  public void setSisaSaldo(List<SisaSaldo> sisaSaldo) {
    this.sisaSaldo = sisaSaldo;
  }

}

package com.example.ktmpintar.models.HistoryKantin;

import com.google.gson.annotations.SerializedName;

public class SisaSaldo {
    @SerializedName("saldo")
    private String saldo;

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }
}

package com.example.ktmpintar.models.HistoryParkir;

import com.google.gson.annotations.SerializedName;

public class HistoryParkirModel {
  @SerializedName("id")
  private int id;

  @SerializedName("mhs_id")
  private int mhs_id;

  @SerializedName("no_kartu")
  private String no_kartu;

  @SerializedName("jam_masuk")
  private String jam_masuk;

  @SerializedName("jam_keluar")
  private String jam_keluar;

  @SerializedName("created_at")
  private String created_at;

  @SerializedName("updated_at")
  private String updated_at;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getMhs_id() {
    return mhs_id;
  }

  public void setMhs_id(int mhs_id) {
    this.mhs_id = mhs_id;
  }

  public String getNo_kartu() {
    return no_kartu;
  }

  public void setNo_kartu(String no_kartu) {
    this.no_kartu = no_kartu;
  }

  public String getJam_masuk() {
    return jam_masuk;
  }

  public void setJam_masuk(String jam_masuk) {
    this.jam_masuk = jam_masuk;
  }

  public String getJam_keluar() {
    return jam_keluar;
  }

  public void setJam_keluar(String jam_keluar) {
    this.jam_keluar = jam_keluar;
  }

  public String getCreated_at() {
    return created_at;
  }

  public void setCreated_at(String created_at) {
    this.created_at = created_at;
  }

  public String getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(String updated_at) {
    this.updated_at = updated_at;
  }
}

package com.example.ktmpintar.models.HistoryParkir;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseHistoryParkir {
  @SerializedName("error")
  private boolean error;

  @SerializedName("Data")
  private List<HistoryParkirModel> data;

  @SerializedName("Status Kendaraan")
  private List<StatusParkir> statusParkir;

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public List<HistoryParkirModel> getData() {
    return data;
  }

  public void setData(List<HistoryParkirModel> data) {
    this.data = data;
  }

  public List<StatusParkir> getStatusParkir() {
    return statusParkir;
  }

  public void setStatusParkir(List<StatusParkir> statusParkir) {
    this.statusParkir = statusParkir;
  }
}

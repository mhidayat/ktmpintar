package com.example.ktmpintar.models.HistoryParkir;

import com.google.gson.annotations.SerializedName;

public class StatusParkir {
  @SerializedName("status")
  private String status;

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }
}

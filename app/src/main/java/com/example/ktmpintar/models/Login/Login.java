package com.example.ktmpintar.models.Login;

import com.google.gson.annotations.SerializedName;

public class Login {

  @SerializedName("Data")
  private LoginData loginData;

  @SerializedName("Error")
  private boolean error;

  @SerializedName("Message")
  private String message;

  public LoginData getLoginData() {
    return loginData;
  }

  public void setLoginData(LoginData loginData) {
    this.loginData = loginData;
  }

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}

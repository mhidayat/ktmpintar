package com.example.ktmpintar.models.Login;

import com.google.gson.annotations.SerializedName;

public class LoginData {
  @SerializedName("id")
  private String id;

  @SerializedName("nama_mhs")
  private String nama_mhs;

  @SerializedName("no_kartu")
  private String no_kartu;

  @SerializedName("nim")
  private String nim;

  @SerializedName("jurusan")
  private String jurusan;

  @SerializedName("saldo")
  private String saldo;

  @SerializedName("token")
  private String token;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getNama_mhs() {
    return nama_mhs;
  }

  public void setNama_mhs(String nama_mhs) {
    this.nama_mhs = nama_mhs;
  }

  public String getNo_kartu() {
    return no_kartu;
  }

  public void setNo_kartu(String no_kartu) {
    this.no_kartu = no_kartu;
  }

  public String getNim() {
    return nim;
  }

  public void setNim(String nim) {
    this.nim = nim;
  }

  public String getJurusan() {
    return jurusan;
  }

  public void setJurusan(String jurusan) {
    this.jurusan = jurusan;
  }

  public String getSaldo() {
    return saldo;
  }

  public void setSaldo(String saldo) {
    this.saldo = saldo;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  @Override
  public String toString() {
    return "LoginData{" +
            "id='" + id + '\'' +
            ", nama_mhs='" + nama_mhs + '\'' +
            ", no_kartu='" + no_kartu + '\'' +
            ", nim='" + nim + '\'' +
            ", jurusan='" + jurusan + '\'' +
            ", saldo='" + saldo + '\'' +
            ", token='" + token + '\'' +
            '}';
  }
}

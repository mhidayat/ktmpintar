package com.example.ktmpintar.models.Login;

import com.google.gson.annotations.SerializedName;

public class LoginDataPenjual {

    @SerializedName("id")
    private String id;

    @SerializedName("penjual_id")
    private String penjual_id;

    @SerializedName("kode_kantin")
    private String kode_kantin;

    @SerializedName("nama_warung")
    private String nama_warung;

    @SerializedName("saldo_penjual")
    private String saldo_penjual;

    @SerializedName("token")
    private String token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_penjual() {
        return penjual_id;
    }

    public void setId_penjual(String penjual) {
        this.penjual_id = penjual_id;
    }

    public String getKode_kantin() {
        return kode_kantin;
    }

    public void setKode_kantin(String kode_kantin) {
        this.kode_kantin = kode_kantin;
    }

    public String getNama_warung() {
        return nama_warung;
    }

    public void setNama_warung(String nama_warung) {
        this.nama_warung = nama_warung;
    }

    public String getSaldo_penjual() {
        return saldo_penjual;
    }

    public void setSaldo_penjual(String saldo_penjual) {
        this.saldo_penjual = saldo_penjual;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "LoginDataPenjual{" +
                "id='" + id + '\'' +
                ", id_penjual='" + penjual_id + '\'' +
                ", kode_kantin='" + kode_kantin + '\'' +
                ", nama_warung='" + nama_warung + '\'' +
                ", saldo_penjual='" + saldo_penjual + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

}

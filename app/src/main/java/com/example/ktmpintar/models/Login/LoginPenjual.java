package com.example.ktmpintar.models.Login;

import com.google.gson.annotations.SerializedName;

public class LoginPenjual {

    @SerializedName("Error")
    private boolean error;

    @SerializedName("Message")
    private String message;

    @SerializedName("Data")
    private LoginDataPenjual loginDataPenjual;

    public LoginDataPenjual getLoginDataPenjual() {
        return loginDataPenjual;
    }

    public void setLoginDataPenjual(LoginDataPenjual loginDataPenjual) {
        this.loginDataPenjual = loginDataPenjual;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}

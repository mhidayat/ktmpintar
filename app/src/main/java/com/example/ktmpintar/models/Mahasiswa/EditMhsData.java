package com.example.ktmpintar.models.Mahasiswa;

import com.google.gson.annotations.SerializedName;

public class EditMhsData {
    @SerializedName("id")
    private String id;

    @SerializedName("nama_mhs")
    private String nama_mhs;

    @SerializedName("nim")
    private String nim;

    @SerializedName("kelas")
    private String kelas;

    @SerializedName("jurusan")
    private String jurusan;

    @SerializedName("no_telfon")
    private String no_telfon;

    @SerializedName("email")
    private String email;

    @SerializedName("token")
    private String token;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama_mhs() {
        return nama_mhs;
    }

    public void setNama_mhs(String nama_mhs) {
        this.nama_mhs = nama_mhs;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getNo_telfon() {
        return no_telfon;
    }

    public void setNo_telfon(String no_telfon) {
        this.no_telfon = no_telfon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "EditMhsData{" +
                "id='" + id + '\'' +
                ", nama_mhs='" + nama_mhs + '\'' +
                ", nim='" + nim + '\'' +
                ", kelas='" + kelas + '\'' +
                ", jurusan='" + jurusan + '\'' +
                ", no_telfon='" + no_telfon + '\'' +
                ", email='" + email + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}

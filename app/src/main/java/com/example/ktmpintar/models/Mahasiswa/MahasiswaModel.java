package com.example.ktmpintar.models.Mahasiswa;

import com.google.gson.annotations.SerializedName;

public class MahasiswaModel {
  @SerializedName("id")
  private int id;

  @SerializedName("no_kartu")
  private String no_kartu;

  @SerializedName("nim")
  private String nim;

  @SerializedName("nama_mhs")
  private String nama_mhs;

  @SerializedName("tempat_lahir")
  private String tempat_lahir;

  @SerializedName("tgl_lahir")
  private String tgl_lahir;

  @SerializedName("kelas")
  private String kelas;

  @SerializedName("jurusan")
  private String jurusan;

  @SerializedName("alamat")
  private String alamat;

  @SerializedName("no_telfon")
  private String no_telfon;

  @SerializedName("email")
  private String email;

  @SerializedName("saldo")
  private String saldo;

  @SerializedName("created_at")
  private String created_at;

  @SerializedName("updated_at")
  private String updated_at;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getNo_kartu() {
    return no_kartu;
  }

  public void setNo_kartu(String no_kartu) {
    this.no_kartu = no_kartu;
  }

  public String getNim() {
    return nim;
  }

  public void setNim(String nim) {
    this.nim = nim;
  }

  public String getNama_mhs() {
    return nama_mhs;
  }

  public void setNama_mhs(String nama_mhs) {
    this.nama_mhs = nama_mhs;
  }

  public String getTempat_lahir() {
    return tempat_lahir;
  }

  public void setTempat_lahir(String tempat_lahir) {
    this.tempat_lahir = tempat_lahir;
  }

  public String getTgl_lahir() {
    return tgl_lahir;
  }

  public void setTgl_lahir(String tgl_lahir) {
    this.tgl_lahir = tgl_lahir;
  }

  public String getKelas() {
    return kelas;
  }

  public void setKelas(String kelas) {
    this.kelas = kelas;
  }

  public String getJurusan() {
    return jurusan;
  }

  public void setJurusan(String jurusan) {
    this.jurusan = jurusan;
  }

  public String getAlamat() {
    return alamat;
  }

  public void setAlamat(String alamat) {
    this.alamat = alamat;
  }

  public String getNo_telfon() {
    return no_telfon;
  }

  public void setNo_telfon(String no_telfon) {
    this.no_telfon = no_telfon;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getSaldo() {
    return saldo;
  }

  public void setSaldo(String saldo) {
    this.saldo = saldo;
  }

  public String getCreated_at() {
    return created_at;
  }

  public void setCreated_at(String created_at) {
    this.created_at = created_at;
  }

  public String getUpdated_at() {
    return updated_at;
  }

  public void setUpdated_at(String updated_at) {
    this.updated_at = updated_at;
  }

  @Override
  public String toString() {
    return "MahasiswaModel{" +
            "id=" + id +
            ", no_kartu='" + no_kartu + '\'' +
            ", nim='" + nim + '\'' +
            ", nama_mhs='" + nama_mhs + '\'' +
            ", tempat_lahir='" + tempat_lahir + '\'' +
            ", tgl_lahir='" + tgl_lahir + '\'' +
            ", kelas='" + kelas + '\'' +
            ", jurusan='" + jurusan + '\'' +
            ", alamat='" + alamat + '\'' +
            ", no_telfon='" + no_telfon + '\'' +
            ", email='" + email + '\'' +
            ", saldo='" + saldo + '\'' +
            ", created_at='" + created_at + '\'' +
            ", updated_at='" + updated_at + '\'' +
            '}';
  }
}

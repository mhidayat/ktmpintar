package com.example.ktmpintar.models.Mahasiswa;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseMahasiswa {
  @SerializedName("Error")
  private boolean error;

  @SerializedName("Message")
  private String message;

  @SerializedName("Data")
  private List<MahasiswaModel> data;

  public boolean isError() {
    return error;
  }

  public void setError(boolean error) {
    this.error = error;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public List<MahasiswaModel> getData() {
    return data;
  }

  public void setData(List<MahasiswaModel> data) {
    this.data = data;
  }
}

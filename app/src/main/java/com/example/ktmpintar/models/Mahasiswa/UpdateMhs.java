package com.example.ktmpintar.models.Mahasiswa;

import com.example.ktmpintar.models.Login.LoginData;
import com.google.gson.annotations.SerializedName;

public class UpdateMhs {

    @SerializedName("Error")
    private boolean error;

    @SerializedName("Message")
    private String message;

    @SerializedName("Data")
    private EditMhsData editMhsData;


    public EditMhsData getEditMhsData() {
        return editMhsData;
    }

    public void setEditMhsData(EditMhsData editMhsData) {
        this.editMhsData = editMhsData;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

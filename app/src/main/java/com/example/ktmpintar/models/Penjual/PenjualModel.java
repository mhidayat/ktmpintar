package com.example.ktmpintar.models.Penjual;

import com.google.gson.annotations.SerializedName;

public class PenjualModel {
    @SerializedName("id")
    private int id;

    @SerializedName("nama_warung")
    private String nama_warung;

    @SerializedName("id_penjual")
    private String id_penjual;

    @SerializedName("jam_transaksi")
    private String jam_transaksi;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamaWarung() {
        return nama_warung;
    }

    public void setNama_warung(String nama_warung) {
        this.nama_warung = nama_warung;
    }

    public String getIdPenjual() {
        return id_penjual;
    }

    public void setIdPenjual(String id_penjual) {
        this.id_penjual = id_penjual;
    }

    public String getJam_transaksi() {
        return jam_transaksi;
    }

    public void setJam_transaksi(String jam_transaksi) {
        this.jam_transaksi = jam_transaksi;
    }

}

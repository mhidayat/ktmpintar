package com.example.ktmpintar.models.Penjual;

import com.example.ktmpintar.models.HistoryKantin.SisaSaldo;
import com.example.ktmpintar.models.Mahasiswa.MahasiswaModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponsePenjual {
    @SerializedName("Error")
    private boolean error;

    @SerializedName("Data")
    private List<PenjualModel> data;

    @SerializedName("Saldo")
    private List<SaldoPenjual> saldoPenjual;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<PenjualModel> getData() {
        return data;
    }

    public void setData(List<PenjualModel> data) {
        this.data = data;
    }

    public List<SaldoPenjual> getSisaSaldoPenjual() {
        return saldoPenjual;
    }

    public void setSisaSaldoPenjual(List<SaldoPenjual> saldoPenjual) {
        this.saldoPenjual = saldoPenjual;
    }

}

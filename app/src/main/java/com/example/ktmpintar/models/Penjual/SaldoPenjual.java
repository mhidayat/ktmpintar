package com.example.ktmpintar.models.Penjual;

import com.google.gson.annotations.SerializedName;

public class SaldoPenjual {
    @SerializedName("saldo_penjual")
    private String saldo;

    public String getSaldoPenjual() {
        return saldo;
    }

    public void setSaldoPenjual(String saldoPenjual) {
        this.saldo = saldoPenjual;
    }
}
